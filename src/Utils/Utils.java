package Utils;

import java.util.List;

public class Utils {
    public static String joinList(List<String> list, String delimiter){
    	StringBuilder joined = new StringBuilder();
    	
        boolean isFirst = true;
        for (String link : list){
            if (!isFirst){
                joined.append(delimiter);
            }
            joined.append(link);
            isFirst = false;
        }
        
        return joined.toString();
    }
}
