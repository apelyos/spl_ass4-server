package Utils;

import protocol_whatsapp.WhatsappEngine;
import protocol_whatsapp.WhatsappProtocol;
import threadPerClient.ConnectionHandler;
import tokenizer_http.HttpTokenizer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Driver {
    private static final int HTTP_PORT = 80;
    public static void main (String args[]){
        ServerSocket lpServerSocket = null;

        // Get port
        int port = HTTP_PORT;

        // Listen on port
        try {
            lpServerSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Couldn't listen on port " + port);
            System.exit(1);
        }

        System.out.println("Listening...");
        Socket lpClientSocket = null;
        WhatsappEngine whatsappEngine = new WhatsappEngine();
        // Waiting for a client connection
        while (true){
            try {
                lpClientSocket = lpServerSocket.accept();
                WhatsappProtocol whatsappProtocol = new WhatsappProtocol(whatsappEngine);
                HttpTokenizer httpTokenizer = new HttpTokenizer();
                (new Thread(new ConnectionHandler<String>(lpClientSocket, whatsappProtocol, httpTokenizer))).start();
                System.out.println("New client connection started");
            } catch (IOException e) {
                System.out.println("Failed to accept...");
                System.exit(1);
            }
        }


//        System.out.println("Accepted connection from client!");
//        System.out.println("The client is from: " + lpClientSocket.getInetAddress() + ":" + lpClientSocket.getPort());
//
//        // Read messages from client
//        BufferedReader in = null;
//        try {
//            in = new BufferedReader(new InputStreamReader(lpClientSocket.getInputStream()));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        String msg;
//
//        try {
//            while ((msg = in.readLine()) != null)
//            {
//                System.out.println("Received from client: " + msg);
//                if (msg.equals("bye"))
//                {
//                    System.out.println("Client sent a terminating message");
//                    break;
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("Client disconnected - bye bye...");
//
//        try {
//            lpServerSocket.close();
//            lpClientSocket.close();
//            in.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}
