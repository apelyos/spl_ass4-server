package tokenizer;

import java.io.IOException;
import java.io.InputStreamReader;

public interface Tokenizer<T> {

   /**
    * Get the next complete message if it exists, advancing the tokenizer to the next message.
    * @return the next complete message, and null if no complete message exist.
    */
   T nextMessage() throws IOException;

   /**
    * @return whether the input stream is still alive.
    */
   boolean isAlive();

   /**
    * adding a bufferedReader from which the tokenizer reads the input.
    */
   void addInputStream(InputStreamReader inputStreamReader);
   
   /**
    * adding the needed tokenizing parameters to the message
    */
   T tokenize(T msg);
}
