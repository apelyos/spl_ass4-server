package tokenizer;

public interface AsyncTokenizerFactory<T> {
   AsyncMessageTokenizer<T> create();
}
