package protocol_http;

public class HttpResponse extends HttpEntity{
    private int statusCode;

    public HttpResponse(int statusCode, String httpVersion) {
        super(httpVersion);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

}
