package protocol_http;

import java.util.HashMap;
import java.util.Map;

public abstract class HttpEntity {
    private String httpVersion;
    private Map<String,String> variables;
    protected String message;

    public HttpEntity(String httpVersion) {
        this.httpVersion = httpVersion;
        variables = new HashMap<String, String>();
    }

    public void setHttpVersion(String httpVersion) {
        this.httpVersion = httpVersion;
    }

    public void addVariable(String key, String value){
        variables.put(key, value);
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setVariables(Map<String, String> variables) {
        this.variables = variables;
    }

    public Map<String, String> getVariables() {
        return variables;
    }

    public String getHttpVersion() {
        return httpVersion;
    }

    public String getMessage() {
        return message;

    }
}
