package protocol_http;

public class HttpRequest extends HttpEntity
{
    public HttpRequest(String httpVersion) {
        super(httpVersion);
    }

    public enum RequestType {GET, POST};
    private RequestType requestType;
    private String requestURI;

    public void setRequestType(String requestType) {
        if (requestType.equals(RequestType.GET.name())){
            this.requestType = RequestType.GET;
        }
        else if (requestType.equals(RequestType.POST.name())){
            this.requestType = RequestType.POST;
        }
        else {
            throw new RuntimeException("Non existing request type: '"+requestType+"'");
        }
    }

    public void setRequestURI(String requestURI) {
        this.requestURI = requestURI;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public String getRequestURI() {
        return requestURI;
    }

    public String getVariable (String key){
        return getVariables().get(key);
    }

}
