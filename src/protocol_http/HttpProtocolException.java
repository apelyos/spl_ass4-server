package protocol_http;

public class HttpProtocolException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -951820075373205626L;

	public HttpProtocolException() {
        super();
    }

    public HttpProtocolException(String message) {
        super(message);
    }
}
