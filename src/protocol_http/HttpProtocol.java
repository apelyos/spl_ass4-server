package protocol_http;

import protocol.AsyncServerProtocol;

import java.util.HashMap;
import java.util.Map;

public abstract class HttpProtocol implements AsyncServerProtocol<String>{
    protected final String HTTP_VERSION = "HTTP/1.1";
    public static final int OK = 200, BAD_REQUEST = 400, FORBIDDEN = 403, NOT_FOUND = 404, METHOD_NOT_ALLOWED = 405,
            VERSION_NOT_SUPPORTED = 505;
    
    public HttpProtocol() {
        
    }
 
    public String processMessage(String message) {
        HttpRequest httpRequest;
        
        // parse request
        try {
            httpRequest = parseRequest(message);
        } catch (Exception e) {
            HttpResponse httpResponse = (new HttpResponse(BAD_REQUEST, HTTP_VERSION));
            httpResponse.setMessage("Bad Request");
            return buildHttpResponse(httpResponse);
        }
        
        // check http-version
        if(!httpRequest.getHttpVersion().equals(HTTP_VERSION)){
            return buildHttpResponse(new HttpResponse(VERSION_NOT_SUPPORTED, HTTP_VERSION));
        }
        
        // call protocol action
        HttpResponse httpResponse = protocolAction(httpRequest);
        
        // return http response
        return buildHttpResponse(httpResponse);
    }

    protected abstract HttpResponse protocolAction(HttpRequest httpRequest);

    private String buildHttpResponse(HttpResponse httpResponse) {
        String endDelimiter = "\r\n";
        String midDelimiter = ":";
        String message = httpResponse.getHttpVersion() + " " + httpResponse.getStatusCode() + endDelimiter;
        Map <String, String> variables = httpResponse.getVariables();
        
        for (String key : variables.keySet()){
            message += key + midDelimiter + variables.get(key) + endDelimiter;
        }
        
        if (httpResponse.getMessage() != null) {
            message += endDelimiter + httpResponse.getMessage() + endDelimiter;
        }

        return message;
    }

    private HttpRequest parseRequest (String request){
        String midDelimiter = ":";
        String endDelimiter = "\r\n";
        HttpRequest httpRequest = new HttpRequest(HTTP_VERSION);

        request = placeHttpRequestInfo (httpRequest, request, endDelimiter); //filling Type, URI, Version
        addHttpVariables(httpRequest, request, midDelimiter, endDelimiter); //filling variables

        return httpRequest;
    }

    private void addHttpVariables(HttpRequest httpRequest, String request, String midDelimiter, String endDelimiter) {
        if (request.contains(endDelimiter+endDelimiter)){
            if(request.indexOf(endDelimiter+endDelimiter)== 0){//no header
                httpRequest.setMessage(request.substring(endDelimiter.length()*2));
            } else {
                String[] headerAndMessage = request.substring(endDelimiter.length()).split(endDelimiter + endDelimiter);
                String header = headerAndMessage[0];
                String message = "";
                if (headerAndMessage.length > 1) { //has body
                	message = headerAndMessage[1];
                }
                 
                httpRequest.setVariables(convertToMap(header, endDelimiter, midDelimiter));
                httpRequest.setMessage(message);
            }
        }
        else {//no body
            request = request.substring(endDelimiter.length());
            httpRequest.setVariables(convertToMap(request, endDelimiter, midDelimiter));
        }
    }

    protected Map<String, String> convertToMap(String headers, String endDelimiter, String midDelimiter) {
        Map<String, String> headerMap = new HashMap<String, String>();
        if (headers == null || headers.isEmpty()){
            return null;
        }
        String[] headerArray = headers.split(endDelimiter);
        for (String header : headerArray){
            String[] splitHeader = header.split(midDelimiter);
            headerMap.put(splitHeader[0], splitHeader[1]);
        }
        return headerMap;
    }

    private String placeHttpRequestInfo(HttpRequest httpRequest, String request, String endDelimiter){
        int start = 0;
        int end = request.indexOf(' ');
        httpRequest.setRequestType(request.substring(start, end));
        start = end+1;
        end = request.indexOf(' ', start);
        httpRequest.setRequestURI(request.substring(start, end));
        start = end+1;
        end = request.indexOf(endDelimiter, start);
        httpRequest.setHttpVersion(request.substring(start, end));
        return request.substring(end, request.length()-endDelimiter.length());
    }

}

