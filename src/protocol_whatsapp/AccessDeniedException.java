package protocol_whatsapp;

public class AccessDeniedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7201550248093457683L;
	
	public AccessDeniedException(String msg) {
		super(msg);
	}

}
