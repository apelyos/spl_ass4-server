package protocol_whatsapp;

import protocol_http.HttpProtocolFactory;

public class WhatsappProtocolFactory extends HttpProtocolFactory {
    WhatsappEngine whatsappEngine;
    
    public WhatsappProtocolFactory (WhatsappEngine whatsappEngine){
        this.whatsappEngine = whatsappEngine;
    }
    
    public WhatsappProtocol create() {
        return new WhatsappProtocol(whatsappEngine);
    }
}
