package protocol_whatsapp;

public class WhatsappException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6092512153018491807L;

	
	public WhatsappException(String msg) {
		super(msg);
	}
}
