package protocol_whatsapp;

import protocol_http.HttpProtocol;
import protocol_http.HttpRequest;
import protocol_http.HttpResponse;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

import Utils.Utils;

public class WhatsappProtocol extends HttpProtocol {
    private WhatsappEngine whatsappEngine;
    private final String DELIM_LINE = "\r\n";
    private final String DELIM = ",";
    private final String BODY_MID_DELIM = "=";
    private final String BODY_END_DELIM = "&";
    private boolean shouldClose;
    
    protected final Map<String, HttpRequest.RequestType> validUris =
            new HashMap<String, HttpRequest.RequestType>(){{
                put("/login.jsp", HttpRequest.RequestType.POST);
                put("/logout.jsp", HttpRequest.RequestType.GET);
                put("/list.jsp", HttpRequest.RequestType.POST);
                put("/create_group.jsp", HttpRequest.RequestType.POST);
                put("/send.jsp", HttpRequest.RequestType.POST);
                put("/add_user.jsp", HttpRequest.RequestType.POST);
                put("/remove_user.jsp", HttpRequest.RequestType.POST);
                put("/queue.jsp", HttpRequest.RequestType.GET);
            }};

    public WhatsappProtocol (WhatsappEngine whatsappEngine) {
        super();
        this.whatsappEngine = whatsappEngine;
        shouldClose = false;
    }

    @Override
    protected HttpResponse protocolAction(HttpRequest httpRequest) {
        String uri = httpRequest.getRequestURI();
        String cookie = getCookie(httpRequest.getVariable("Cookie"));
        
        Map <String,String> body;
        try {
        	body = convertToMapEncoded(httpRequest.getMessage(), BODY_END_DELIM, BODY_MID_DELIM);
        } catch (Exception e) {
        	return new HttpResponse(BAD_REQUEST, HTTP_VERSION);
        }
        
        try {
        	
            if (uri.equals("/login.jsp")) {
                return login(body);
            } else if (uri.equals("/logout.jsp")) {
                return logout(cookie);
            } else if (uri.equals("/list.jsp")) {
                return list(body, cookie);
            } else if (uri.equals("/create_group.jsp")) {
                return createGroup(body, cookie);
            } else if (uri.equals("/send.jsp")) {
                return send(body, cookie);
            } else if (uri.equals("/add_user.jsp")) {
                return addUser(body, cookie);
            } else if (uri.equals("/remove_user.jsp")) {
                return removeUser(body, cookie);
            } else if (uri.equals("/queue.jsp")) {
                return queue(cookie);
            } else {
            	HttpResponse httpResponse = new HttpResponse(NOT_FOUND, HTTP_VERSION);
                httpResponse.setMessage("Not Found");
                return httpResponse;
            }
            
        } catch (WhatsappException e) {
        	HttpResponse httpResponse = new HttpResponse(OK, HTTP_VERSION);
        	httpResponse.setMessage(e.getMessage());
        	return httpResponse;
        } catch (AccessDeniedException e) {
        	HttpResponse httpResponse = new  HttpResponse(FORBIDDEN, HTTP_VERSION);
        	httpResponse.setMessage("Forbidden");
        	return httpResponse;
        } catch (Exception e){
        	HttpResponse httpResponse = new HttpResponse(BAD_REQUEST, HTTP_VERSION);
        	httpResponse.setMessage("Bad Request");
        	return httpResponse;
        }
    }

    private String getCookie(String cookie) {
        if (cookie == null){
            return null;
        }
        
        String decoded;
        try {
        	decoded = URLDecoder.decode(cookie.substring(cookie.indexOf("=") + 1),"UTF-8");
        } catch (Exception e) {
        	return null;
        }
        
        return decoded;
    }

    private HttpResponse logout(String cookie) throws AccessDeniedException, WhatsappException {
        HttpResponse httpResponse = new HttpResponse(OK, HTTP_VERSION);
        whatsappEngine.logout(cookie);
        shouldClose = true;
        httpResponse.setMessage("Goodbye.");
        return  httpResponse;
    }

    private HttpResponse list(Map <String,String> body, String cookie) throws AccessDeniedException, WhatsappException {
        HttpResponse httpResponse = new HttpResponse(OK, HTTP_VERSION);
        String listType = body.get("List");
        if (listType.equals("Users")) {
            List<String> userList = whatsappEngine.listUsers(cookie);
            httpResponse.setMessage(Utils.joinList(userList, DELIM_LINE));
        } else if (listType.equals("Groups")) {
            List<String> groupsList = whatsappEngine.listGroups(cookie);
            httpResponse.setMessage(Utils.joinList(groupsList, DELIM_LINE));
        } else if (listType.equals("Group")) {
            String groupList = whatsappEngine.listGroup(cookie, body.get("Group"));
            httpResponse.setMessage(groupList);
        } else {
            httpResponse.setMessage("ERROR 273: Missing Parameters");
        }
        return  httpResponse;
    }

    private HttpResponse createGroup(Map <String,String> body, String cookie) throws AccessDeniedException, WhatsappException {
        HttpResponse httpResponse = new HttpResponse(OK, HTTP_VERSION);
        List<String> users = Arrays.asList(body.get("Users").split(DELIM));
        whatsappEngine.createGroup(cookie, body.get("GroupName"), users);
        httpResponse.setMessage("Group " + body.get("GroupName") + " Created");
        return  httpResponse;
    }

    private HttpResponse send(Map <String,String> body, String cookie) throws AccessDeniedException, WhatsappException {
        HttpResponse httpResponse = new HttpResponse(OK, HTTP_VERSION);
        String type = body.get("Type");
        if (type.equals("Group")){
            whatsappEngine.sendMessageToGroup(cookie, body.get("Target"),
                    body.get("Content"));
        } else if (type.equals("Direct")){
            whatsappEngine.sendMessageToUser(cookie, body.get("Target"),
                    body.get("Content"));
        } else {
            httpResponse.setMessage("ERROR 836: Invalid Type");
        }
        return httpResponse;
    }

    private HttpResponse addUser(Map <String,String> body, String cookie) throws AccessDeniedException, WhatsappException {
        HttpResponse httpResponse = new HttpResponse(OK, HTTP_VERSION);
        whatsappEngine.addUserToGroup(cookie, body.get("Target"),
                body.get("User"));
        httpResponse.setMessage(body.get("User")+" added to " + body.get("Target"));
        return  httpResponse;
    }

    private HttpResponse removeUser(Map <String,String> body, String cookie) throws AccessDeniedException, WhatsappException {
        HttpResponse httpResponse = new HttpResponse(OK, HTTP_VERSION);
        whatsappEngine.removeUserFromGroup(cookie, body.get("Target"),
                body.get("User"));
        httpResponse.setMessage(body.get("User")+" removed from " + body.get("Target"));
        return  httpResponse;
    }

    private HttpResponse queue(String cookie) throws AccessDeniedException, WhatsappException {
        HttpResponse httpResponse = new HttpResponse(OK, HTTP_VERSION);
        List<String> messages = whatsappEngine.dequeueMessages(cookie);
        if (messages == null || messages.isEmpty()){
            httpResponse.setMessage("No new messages");
        } else {
            httpResponse.setMessage(Utils.joinList(messages, DELIM_LINE));
        }
        return  httpResponse;
    }

    private HttpResponse login(Map<String, String> body) throws AccessDeniedException, WhatsappException, UnsupportedEncodingException {
        HttpResponse httpResponse = new HttpResponse(OK, HTTP_VERSION);
        String cookie = whatsappEngine.login(body.get("UserName"), body.get("Phone"));
        httpResponse.addVariable("Set-Cookie", " user_auth="+ URLEncoder.encode(cookie,"UTF-8"));
        httpResponse.setMessage("Welcome " + body.get("UserName") + "@" + body.get("Phone"));
        return  httpResponse;
    }

    public boolean isEnd(String msg) {
    	return shouldClose;
    }

    protected int validateUri(HttpRequest httpRequest) {
        String uri = httpRequest.getRequestURI();
        HttpRequest.RequestType type = httpRequest.getRequestType();
        if (validUris.keySet().contains(uri)){
            if (validUris.get(uri).equals(type)) {
                return OK;
            }
            return METHOD_NOT_ALLOWED;
        }
        return NOT_FOUND;
    }

    protected Map<String, String> convertToMapEncoded (String headers, String endDelimiter, String midDelimiter) throws UnsupportedEncodingException {
        Map<String, String> headerMap = new HashMap<String, String>();
        if (headers == null || headers.isEmpty()){
            return null;
        }
        
        String[] headerArray = headers.split(endDelimiter);
        for (String header : headerArray){
            String[] splitHeader = header.split(midDelimiter);
            headerMap.put(splitHeader[0], URLDecoder.decode(splitHeader[1],"UTF-8"));
        }
        return headerMap;
    }

	@Override
	public boolean shouldClose() {
		return shouldClose;
	}

	@Override
	public void connectionTerminated() {
		shouldClose = true;
	}
}
