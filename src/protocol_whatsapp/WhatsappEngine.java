package protocol_whatsapp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class WhatsappEngine {
	private static final Logger log = Logger.getLogger(WhatsappEngine.class.getName());
	private final String NOT_AUTH_ERR = "Not Authenticated.";
	ConcurrentHashMap<String,WhatsappUser> _users;
	ConcurrentHashMap<String,WhatsappGroup> _groups;
	
	public WhatsappEngine() {
		_users = new ConcurrentHashMap<String,WhatsappUser>(); // keys: phones
		_groups = new ConcurrentHashMap<String,WhatsappGroup>(); // keys: group name
	}
	
	// Returns token
	public String login(String userName, String phoneNum) throws WhatsappException, AccessDeniedException {
		if (userName.isEmpty() || phoneNum.isEmpty()) throw new WhatsappException ("ERROR 765: Cannot login, missing parameters");
		
		synchronized (_users) {
			if (isUserExist(phoneNum)) throw new WhatsappException ("ERROR 766: Target phone number already exists.");
			
			WhatsappSecurityToken token = new WhatsappSecurityToken();
			String securityToken = token.newToken(phoneNum);
			WhatsappUser user = new WhatsappUser(userName, phoneNum);
			_users.put(phoneNum, user);
			log.info("User: " + userName + " logged in.");
			return securityToken;
		}
	}
	
	// Used to revoke authentication
	public void logout(String securityToken) throws WhatsappException, AccessDeniedException  {
		if (!isAuthenticated(securityToken)) {
			throw new AccessDeniedException(NOT_AUTH_ERR);
		}
		
		synchronized (_users) {
			_users.remove(getPhoneFromToken(securityToken));
		}
		
		log.info("User logged out.");

		//TODO: Remove user from groups
	}
	
	// Used to list current users and groups
	public List<String> listUsers(String securityToken)  throws WhatsappException, AccessDeniedException {
		if (!isAuthenticated(securityToken)) {
			throw new AccessDeniedException(NOT_AUTH_ERR);
		}
		
		List<String> result = new ArrayList<String>();
		
		for (WhatsappUser user : _users.values()) {
			result.add(user.toString());
		}
		
		return result;
	}
	
	public List<String> listGroups(String securityToken)  throws WhatsappException, AccessDeniedException {
		if (!isAuthenticated(securityToken)) {
			throw new AccessDeniedException(NOT_AUTH_ERR);
		}
		
		List<String> result = new ArrayList<String>();
		
		for (WhatsappGroup group : _groups.values()) {
			result.add(group.toStringWithPrefix());
		}
		
		return result;
	}

	public String listGroup(String securityToken, String groupName)  throws WhatsappException, AccessDeniedException {
		if (!isAuthenticated(securityToken)) {
			throw new AccessDeniedException(NOT_AUTH_ERR);
		}
		
		if (groupName.isEmpty() || _groups.get(groupName)==null) throw new WhatsappException ("ERROR 273: Missing Parameters");
		if (!isGroupExist(groupName)) throw new WhatsappException ("ERROR 274: Target Does not exist");
		
		WhatsappGroup whatsappGroup = _groups.get(groupName);

		return whatsappGroup.toString();
	}
	
	public void createGroup(String securityToken, String groupName, List<String> userNames)  throws WhatsappException, AccessDeniedException {
		if (!isAuthenticated(securityToken)) {
			throw new AccessDeniedException(NOT_AUTH_ERR);
		}
		
		if (groupName.isEmpty() || userNames == null) throw new WhatsappException ("ERROR 675: Cannot create group, missing parameters");
		synchronized (_groups) {
			if (isGroupExist(groupName)) throw new WhatsappException ("ERROR 511: Group Name Already Taken");
			
			List<WhatsappUser> usersToAdd = new ArrayList<WhatsappUser>();
			
			for (String name : userNames) {
				WhatsappUser user = getUserByName(name);
				if (user == null) throw new WhatsappException ("ERROR 929: Unknown User " + name);
				usersToAdd.add(user);
			}
			
			WhatsappGroup group = new WhatsappGroup(groupName, usersToAdd);
			_groups.put(groupName,group);
		}
	}
	
	public void sendMessageToUser(String securityToken, String targetUserPhoneNum, String message)  throws WhatsappException, AccessDeniedException {
		if (!isAuthenticated(securityToken)) {
			throw new AccessDeniedException(NOT_AUTH_ERR);
		}
		
		if (targetUserPhoneNum.isEmpty() || message.isEmpty()) throw new WhatsappException ("ERROR 711: Cannot send, missing parameters");
		if (!isUserExist(targetUserPhoneNum)) throw new WhatsappException ("ERROR 771: Target Does not Exist");
		
		String phoneNum = getPhoneFromToken(securityToken);
		
		getUserByPhone(targetUserPhoneNum).sendMessage("From: " + phoneNum + "\r\nMsg: " + message);
		
		log.info("Message: \"" + message + "\" sent from: " + phoneNum + " to: " + targetUserPhoneNum);
	}
	
	public void sendMessageToGroup(String securityToken, String targetGroup, String message)  throws WhatsappException, AccessDeniedException {
		if (!isAuthenticated(securityToken)) {
			throw new AccessDeniedException(NOT_AUTH_ERR);
		}
		
		if (targetGroup.isEmpty() || message.isEmpty()) throw new WhatsappException ("ERROR 711: Cannot send, missing parameters");
		if (!isGroupExist(targetGroup)) throw new WhatsappException ("ERROR 771: Target Does not Exist");
		
		String phoneNum = getPhoneFromToken(securityToken);
		
		getGroupByName(targetGroup).sendMessage("From: " + targetGroup + "\r\nMsg: " + message);
		
		log.info("Message: \"" + message + "\" sent from: " + phoneNum + " to group: " + targetGroup);
	}
	
	public void addUserToGroup(String securityToken, String groupName, String userPhoneNum)  throws WhatsappException, AccessDeniedException {
		if (!isAuthenticated(securityToken)) {
			throw new AccessDeniedException(NOT_AUTH_ERR);
		}
		
		if (groupName.isEmpty() || userPhoneNum.isEmpty()) throw new WhatsappException ("ERROR 242: Cannot add user, missing parameters");
		if (!isUserExist(userPhoneNum)) throw new WhatsappException ("ERROR 770: Target Does not Exist");
		if (!isGroupExist(groupName)) throw new WhatsappException ("ERROR 770: Target Does not Exist");
		
		getGroupByName(groupName).addUser(getUserByPhone(userPhoneNum));
	}
	
	public void removeUserFromGroup(String securityToken, String groupName, String userPhoneNum)  throws WhatsappException, AccessDeniedException {
		if (!isAuthenticated(securityToken)) {
			throw new AccessDeniedException(NOT_AUTH_ERR);
		}
		
		if (groupName.isEmpty() || userPhoneNum.isEmpty()) throw new WhatsappException ("ERROR 336: Cannot remove, missing parameters");
		if (!isUserExist(userPhoneNum)) throw new WhatsappException ("ERROR 769: Target does not exist");
		if (!isGroupExist(groupName)) throw new WhatsappException ("ERROR 769: Target does not exist");
		
		getGroupByName(groupName).removeUser(getUserByPhone(userPhoneNum));
	}
	
	public List<String> dequeueMessages(String securityToken)  throws WhatsappException, AccessDeniedException {
		if (!isAuthenticated(securityToken)) {
			throw new AccessDeniedException(NOT_AUTH_ERR);
		}
		
		return getUserByPhone(getPhoneFromToken(securityToken)).getMessages();
	}
	
	private boolean isAuthenticated(String securityToken) throws WhatsappException, AccessDeniedException {
		return isUserExist(getPhoneFromToken(securityToken)); // Validates token + user
	}
	
	private String getPhoneFromToken(String securityToken) throws WhatsappException, AccessDeniedException {
		WhatsappSecurityToken token = new WhatsappSecurityToken(securityToken);
		return token.getPhoneNum();
	}
	
	private WhatsappGroup getGroupByName(String name) {
		return _groups.get(name);
	}
	
	private WhatsappUser getUserByPhone(String phone) {
		return _users.get(phone);
	}
	
	private WhatsappUser getUserByName(String userName) throws WhatsappException {
		for (WhatsappUser user : _users.values()) {
			if (user.getName().equals(userName)) return user;
		}
		
		return null;
	}
	
	private boolean isUserExist(String phoneNum) {
		return _users.containsKey(phoneNum);
	}
	
	private boolean isGroupExist(String groupName) {
		return _groups.containsKey(groupName);
	}
}
