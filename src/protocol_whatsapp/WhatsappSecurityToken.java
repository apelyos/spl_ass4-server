package protocol_whatsapp;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;


class WhatsappSecurityToken {
	private String _key = "713kUoXOlb5LmZ3I"; // DONT CHANGE THIS -- 128 bit key 
	private String _securityToken;
	
	public WhatsappSecurityToken() {
		_securityToken = "";
	}
	
	public WhatsappSecurityToken(String token) {
		_securityToken = token;
    }
	
	public String newToken(String phoneNum) throws AccessDeniedException {
		_securityToken = encrypt(phoneNum);
		return getToken();
	}
	
	public String getToken() {
		return _securityToken;
	}
	
	public String getPhoneNum() throws AccessDeniedException {
		return decrypt(_securityToken);
	}
	
	private String encrypt(String text) throws AccessDeniedException  {
		try {
			// Create key and cipher
			Key aesKey = new SecretKeySpec(_key.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			
			// encrypt the text
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(text.getBytes());
			return (new String(toBase64(encrypted)));
		} catch (Exception e) {
			throw new AccessDeniedException("Invalid token: " + e.getMessage());
		}
	}
	
	private String decrypt(String token) throws AccessDeniedException  {
		try {
			// Create key and cipher
			Key aesKey = new SecretKeySpec(_key.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			
			// decrypt the text
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			String decrypted = new String(cipher.doFinal(fromBase64(token)));
			return(decrypted);
		} catch (Exception e) {
			throw new AccessDeniedException("Invalid token: " + e.getMessage());
		}
	}
	
	private String toBase64(byte[] bytes) {
		return DatatypeConverter.printBase64Binary(bytes);
	}
	
	private byte[] fromBase64(String str) {
		return DatatypeConverter.parseBase64Binary(str);
	}
}
