package protocol_whatsapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import Utils.Utils;

class WhatsappGroup {
	private final String DELIM = ":";
	private final String DELIML = ",";
	private List<WhatsappUser> _users;
	private String _name;
	
	public WhatsappGroup(String name, List<WhatsappUser> users) {
		if (users == null) {
			_users = new Vector<WhatsappUser>();
		} else {
			_users = users;
		}
		
		_name = name;
	}
	
	public synchronized void removeUser(WhatsappUser user) {
		_users.remove(user);
	}
	
	public String toString() {
		List<String> users = new ArrayList<String>();
		
		for (WhatsappUser user : _users) {
			users.add(user.getPhoneNum());
		}
		
		return (Utils.joinList(users, DELIML));
	}

	public String toStringWithPrefix(){
		return (_name + DELIM + toString());
	}
	
	public synchronized void sendMessage(String msg) {
		for (WhatsappUser user : _users) {
			user.sendMessage(msg);
		}
	}
	
	public synchronized void addUser(WhatsappUser user) throws WhatsappException {
		if (userExists(user)) throw new WhatsappException("ERROR 142: Cannot add user, user already in group");
		_users.add(user);
	}
	
	private boolean userExists(WhatsappUser user) {
		return _users.contains(user);
	}
}
