package protocol_whatsapp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

class WhatsappUser {
	private String _userName;
	private String _phoneNum;
	private Date _loginTime;
	private BlockingQueue<String> _messages; 
	private final String DELIM = "@";
	
	public WhatsappUser(String userName, String phoneNum) {
		_messages = new LinkedBlockingQueue<String>();
		_loginTime = new Date();
		_phoneNum = phoneNum;
		_userName = userName;
	}
	
	public synchronized List<String> getMessages() {
		List<String> result = new ArrayList<String>();
		
		while (!_messages.isEmpty()) {
			result.add(_messages.poll());
		}
		
		return result;
	}
	
	public synchronized void sendMessage(String message) {
		_messages.add(message);
	}
	
	public String toString() {
		return (_userName + DELIM + _phoneNum);
	}
	
	public String getPhoneNum() {
		return _phoneNum;
	}
	
	public String getName() {
		return _userName;
	}
	
	public Date getLoginTime() {
		return _loginTime;
	}
}
