package tokenizer_http;
import java.io.IOException;
import java.io.InputStreamReader;

import tokenizer.Tokenizer;

public class HttpTokenizer implements Tokenizer<String> {
    public final char DEFAULT_DELIM = '$';
    public final char _delimiter;
    private InputStreamReader _isr;
    private boolean _closed;

    public HttpTokenizer(){
        _delimiter = DEFAULT_DELIM;
        _closed = false;
    }

    public HttpTokenizer (InputStreamReader isr, char delimiter) {
        _delimiter = delimiter;
        _isr = isr;
        _closed = false;
    }

    @Override
    public void addInputStream(InputStreamReader inputStreamReader) {
        _isr = inputStreamReader;
    }

    public String nextMessage() throws IOException {
        if (!isAlive())
            throw new IOException("tokenizer is closed");

        String ans = null;
        try {
            // we are using a blocking stream, so we should always end up
            // with a message, or with an exception indicating an error in
            // the connection.
            int c;
            StringBuilder sb = new StringBuilder();
            // read char by char, until encountering the framing character, or
            // the connection is closed.
            while ((c = _isr.read()) != -1) {
                if (c == _delimiter)
                    break;

                sb.append((char) c);
            }

            if (c == -1) {
                _closed = true;
            }

            ans = sb.toString();
        } catch (IOException e) {
            _closed = true;
            throw new IOException("Connection is dead");
        }
        return ans;
    }

    public boolean isAlive() {
        return !_closed;
    }

    public String tokenize(String msg) {
    	return msg.concat(String.valueOf(_delimiter));
    }

}
