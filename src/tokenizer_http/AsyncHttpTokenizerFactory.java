package tokenizer_http;

import java.nio.charset.StandardCharsets;

import tokenizer.AsyncMessageTokenizer;
import tokenizer.AsyncTokenizerFactory;

public class AsyncHttpTokenizerFactory implements AsyncTokenizerFactory<String>{
	@Override
	public AsyncMessageTokenizer<String> create() {
		return new AsyncHttpTokenizer("$", StandardCharsets.UTF_8);
	}
}
